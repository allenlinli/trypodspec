//
//  Object.h
//  TryPodspec
//
//  Created by Allen and Kim on 2019/4/23.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Object : NSObject
+ (void)printThis;
@end

NS_ASSUME_NONNULL_END
