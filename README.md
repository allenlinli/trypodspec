# TryPodspec

[![CI Status](https://img.shields.io/travis/allenlinli@gmail.com/TryPodspec.svg?style=flat)](https://travis-ci.org/allenlinli@gmail.com/TryPodspec)
[![Version](https://img.shields.io/cocoapods/v/TryPodspec.svg?style=flat)](https://cocoapods.org/pods/TryPodspec)
[![License](https://img.shields.io/cocoapods/l/TryPodspec.svg?style=flat)](https://cocoapods.org/pods/TryPodspec)
[![Platform](https://img.shields.io/cocoapods/p/TryPodspec.svg?style=flat)](https://cocoapods.org/pods/TryPodspec)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

TryPodspec is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'TryPodspec'
```

## Author

allenlinli@gmail.com, allenlinli@gmail.com

## License

TryPodspec is available under the MIT license. See the LICENSE file for more info.
